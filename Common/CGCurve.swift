//
//  Curve.swift
//  Geometry_iOS
//
//  Created by Igor Voynov on 16.03.2018.
//  Copyright © 2018 igyo. All rights reserved.
//

import Foundation
import CoreGraphics

public typealias CGCurveControlsPoints = (cp1: CGPoint, cp2: CGPoint)

open class CGCurve {
  
  private var firstControlPoints: [CGPoint?] = []
  private var secondControlPoints: [CGPoint?] = []
  
  public init() {}
  
  private func getCubicCoord(start: CGFloat, end: CGFloat,
                             cp1: CGFloat, cp2: CGFloat,
                             distance: CGFloat) -> CGFloat {
    let distance_: CGFloat = (1.0 - distance)
    return start * pow(distance_, 3)
      + 3.0 * cp1 * pow(distance_, 2) * distance
      + 3.0 * cp2 * distance_ * pow(distance, 2)
      + end * pow(distance, 3)
  }
  
  public func getPoint(for value: CGFloat, in axis: CGAxis, points: [CGPoint]) -> CGPoint? {
    let segments = getCurveSegments(dataPoints: points)
    var values: [CGFloat] = []
    switch axis {
    case .x:
      values = points.map {$0.x} + segments.map {$0.cp1.x} + segments.map {$0.cp2.x}
    case .y:
      values = points.map {$0.y} + segments.map {$0.cp1.y} + segments.map {$0.cp2.y}
    }
    guard let min = values.min(), let max = values.max(), value >= min && value <= max else {
      return nil
    }
    
    var point: CGPoint? = nil
    for (index, segment) in segments.enumerated() {
      let startPoint = points[index]
      let endPoint = points[index+1]
      
      var distance: CGFloat
      switch axis {
      case .x:
        distance = (value - startPoint.x) / (endPoint.x - startPoint.x)
      case .y:
        distance = (value - startPoint.y) / (endPoint.y - startPoint.y)
      }
      distance = abs(distance)
      if distance > 1 {
        continue
      }
      
      let x = getCubicCoord(start: startPoint.x, end: endPoint.x,
                            cp1: segment.cp1.x, cp2: segment.cp2.x,
                            distance: distance)
      let y = getCubicCoord(start: startPoint.y, end: endPoint.y,
                            cp1: segment.cp1.y, cp2: segment.cp2.y,
                            distance: distance)
      point = CGPoint(x: x, y: y)
      break
    }
    return point
  }
  
  public func getCurveSegments(dataPoints: [CGPoint]) -> [CGCurveControlsPoints] {
    
    let count = dataPoints.count - 1
    
    if count == 1 {
      let P0 = dataPoints[0]
      let P3 = dataPoints[1]

      let P1x = (2*P0.x + P3.x) / 3
      let P1y = (2*P0.y + P3.y) / 3
      firstControlPoints.append(CGPoint(x: P1x, y: P1y))
      
      let P2x = (2*P1x - P0.x)
      let P2y = (2*P1y - P0.y)
      secondControlPoints.append(CGPoint(x: P2x, y: P2y))
    } else {
      
      firstControlPoints = Array(repeating: nil, count: count)
      var rhsArray: [CGPoint] = []
      var a: [CGFloat] = [], b: [CGFloat] = [], c: [CGFloat] = []
      
      for i in 0..<count {
        var rhsValueX: CGFloat = 0, rhsValueY: CGFloat = 0
        let P0 = dataPoints[i], P3 = dataPoints[i+1]
        
        if i==0 {
          a.append(0)
          b.append(2)
          c.append(1)
          rhsValueX = P0.x + 2*P3.x
          rhsValueY = P0.y + 2*P3.y
        } else if i == count-1 {
          a.append(2)
          b.append(7)
          c.append(0)
          rhsValueX = 8*P0.x + P3.x
          rhsValueY = 8*P0.y + P3.y
        } else {
          a.append(1)
          b.append(4)
          c.append(1)
          rhsValueX = 4*P0.x + 2*P3.x
          rhsValueY = 4*P0.y + 2*P3.y
        }
        rhsArray.append(CGPoint(x: rhsValueX, y: rhsValueY))
      }
      
      // Solve Ax=B. Use Tridiagonal matrix algorithm a.k.a Thomas Algorithm
      for i in 1..<count {
        let rhsValueX = rhsArray[i].x
        let rhsValueY = rhsArray[i].y
        let prevRhsValueX = rhsArray[i-1].x
        let prevRhsValueY = rhsArray[i-1].y
        
        let m = a[i]/b[i-1]
        let b1 = b[i] - m * c[i-1]
        b[i] = b1
        let r2x = rhsValueX - m * prevRhsValueX
        let r2y = rhsValueY - m * prevRhsValueY
      
        rhsArray[i] = CGPoint(x: r2x, y: r2y)
      }
      
      let lastControlPointX = rhsArray[count-1].x/b[count-1]
      let lastControlPointY = rhsArray[count-1].y/b[count-1]
      
      firstControlPoints[count-1] = CGPoint(x: lastControlPointX, y: lastControlPointY)
      
      var i=count-2
      while i >= 0 {
        if let nextControlPoint = firstControlPoints[i+1] {
          let controlPointX = (rhsArray[i].x - c[i] * nextControlPoint.x)/b[i]
          let controlPointY = (rhsArray[i].y - c[i] * nextControlPoint.y)/b[i]
          firstControlPoints[i] = CGPoint(x: controlPointX, y: controlPointY)
        }
        i -= 1
      }
      
      for i in 0..<count {
        if i == count-1 {
          let P3 = dataPoints[i+1]
          guard let P1 = firstControlPoints[i] else { continue }
          
          let controlPointX = (P3.x + P1.x)/2
          let controlPointY = (P3.y + P1.y)/2
          secondControlPoints.append(CGPoint(x: controlPointX, y: controlPointY))
        } else {
          let P3 = dataPoints[i+1]
          guard let nextP1 = firstControlPoints[i+1] else { continue }
          
          let controlPointX = 2*P3.x - nextP1.x
          let controlPointY = 2*P3.y - nextP1.y
          secondControlPoints.append(CGPoint(x: controlPointX, y: controlPointY))
        }
      }
    }
    
    var controlPoints: [CGCurveControlsPoints] = []
    
    for i in 0..<count {
      if firstControlPoints.count > i, let firstControlPoint = firstControlPoints[i],
        secondControlPoints.count > i, let secondControlPoint = secondControlPoints[i] {
        controlPoints.append((firstControlPoint, secondControlPoint))
      }
    }
    
    return controlPoints
  }
}
