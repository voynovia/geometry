//
//  CGFloat+float.swift
//  Geometry_iOS
//
//  Created by Igor Voynov on 17.03.2018.
//

import Foundation
import CoreGraphics

public extension CGFloat {
  var float: Float {
    return Float(self)
  }
}

public extension Float {
  var cgFloat: CGFloat {
    return CGFloat(self)
  }
}
