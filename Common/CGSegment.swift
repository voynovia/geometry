//
//  Segment.swift
//  Geometry_iOS
//
//  Created by Igor Voynov on 16.03.2018.
//  Copyright © 2018 igyo. All rights reserved.
//

import Foundation
import CoreGraphics

public protocol GeometrySegment {
  var begin: CGPoint { get set }
  var end: CGPoint { get set }
}

extension GeometrySegment {
  
  public var length: CGFloat {
    return sqrt(pow(begin.x - end.x, 2) + pow(begin.y - end.y, 2))
  }
  
  public var middle: CGPoint {
    let x = (begin.x + end.x) / 2
    let y = (begin.y + end.y) / 2
    return CGPoint(x: x, y: y)
  }
  
  public func getPoint(with ratio: CGFloat) -> CGPoint {
    let x = (end.x + ratio * begin.x) / (1 + ratio)
    let y = (end.y + ratio * begin.y) / (1 + ratio)
    return CGPoint(x: x, y: y)
  }
  
  public func getPoint(for value: CGFloat, by axis: CGAxis) -> CGPoint? {
    switch axis {
    case .x:
      guard let max = [begin.x, end.x].max(), let min = [begin.x, end.x].min(),
        value >= min && value <= max else { return nil }
      var y: CGFloat
      if begin.x == end.x {
        y = (begin.y + end.y) / 2
      } else {
        y = (value-begin.x) * (begin.y-end.y) / (begin.x-end.x) + begin.y
      }
      return CGPoint(x: value, y: y)
    case .y:
      guard let max = [begin.y, end.y].max(), let min = [begin.y, end.y].min(),
        value >= min && value <= max else { return nil }
      var x: CGFloat
      if begin.y == end.y {
        x = (begin.x + end.x) / 2
      } else {
        x = (value-begin.y) * (begin.x-end.x) / (begin.y-end.y) + begin.x
      }
      return CGPoint(x: x, y: value)
    }
  }
  
}

open class CGSegment: GeometrySegment, Equatable {
  
  public var begin: CGPoint
  public var end: CGPoint
  
  public init(begin: CGPoint, end: CGPoint) {
    self.begin = begin
    self.end = end
  }
  
  public static func == (lhs: CGSegment, rhs: CGSegment) -> Bool {
    return lhs.begin == rhs.begin && lhs.end == rhs.end
  }
  
  public static func getSegments(from points: [CGPoint]) -> [CGSegment] {
    return Array<Int>(1..<points.count).map { CGSegment(begin: points[$0-1], end: points[$0]) }
  }
}
