//
//  Geometry.swift
//  Geometry_iOS
//
//  Created by Igor Voynov on 16.03.2018.
//  Copyright © 2018 igyo. All rights reserved.
//

import Foundation
import CoreGraphics

open class Geometry {
  
  public init() {}
  
  // compute point of of a rectangular triangle
  // C: point with straight angle
  // bc: side of the triangle between C and computed point
  public func getTrianglePoint(c: CGPoint, a: CGPoint, bc: CGFloat) -> CGPoint {
    let ac = CGSegment(begin: c, end: a).length
    let x = c.x + (((a.y - c.y) / ac) * bc)
    let y = c.y + (-((a.x - c.x) / ac) * bc)
    return CGPoint(x: x, y: y)
  }
    
  // compute relation
  public func getRatio(for between: CGFloat, begin: CGFloat, end: CGFloat) -> CGFloat {
    return (end - begin) / (between - begin) - 1
  }
  
  public func getValue(for value: CGFloat, axis: CGAxis, points: [[CGPoint]]) -> CGFloat? {
    var result: CGPoint?
    for points in points {
      if points.count > 2 {
        if let point = CGCurve().getPoint(for: value, in: axis, points: points) {
          result = point
          break
        }
      } else if points.count == 2 {
        if let point = CGSegment(begin: points[0], end: points[1]).getPoint(for: value, by: axis) {
          result = point
          break
        }
      }
    }
    switch axis {
    case .x: return result?.y
    case .y: return result?.x
    }
  }
}
