//
//  Geometry_Mac.h
//  Geometry_Mac
//
//  Created by Igor Voynov on 16.03.2018.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Geometry_Mac.
FOUNDATION_EXPORT double Geometry_MacVersionNumber;

//! Project version string for Geometry_Mac.
FOUNDATION_EXPORT const unsigned char Geometry_MacVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Geometry_Mac/PublicHeader.h>
